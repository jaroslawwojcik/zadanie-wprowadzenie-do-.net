﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Multisystem
{
    class Person
    {
        public string Imie;
        public string Nazwisko;
        public int Wiek;

        public Person(string imie, string nazwisko, int wiek)
        {
            Imie = imie;
            Nazwisko = nazwisko;
            Wiek = wiek;
        }
    }
}
