﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Multisystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Podaj swoje imie");
            string imie = Console.ReadLine();
            Console.WriteLine("Podaj swoje nazwisko");
            string nazwisko = Console.ReadLine();
            Console.WriteLine("Podaj date urodzenia w formacie rrrr-mm-dd");
            DateTime dataUrodzenia = DateTime.Parse(Console.ReadLine());
            DateTime today = DateTime.Today;
            int wiek =  today.Year - dataUrodzenia.Year;
            
            Person osoba = new Person(imie, nazwisko, wiek);
            Console.WriteLine(osoba.Imie);

            JObject o = (JObject)JToken.FromObject(osoba);

            Console.WriteLine(o.ToString());
           
            Console.ReadKey();
        }
    }
}
